extern crate gl;
extern crate glfw;

use super::context::*;
use crate::render::*;

use std::cell::RefCell;

// TODO Move every unsafe block in this file to separate functions.
// TODO Build the context immediately so it's simpler.

pub struct World<T> {
    glfw: Option<RefCell<glfw::Glfw>>,
    window: Option<RefCell<glfw::Window>>,
    events: Option<RefCell<std::sync::mpsc::Receiver<(f64, glfw::WindowEvent)>>>,
    default_shader: Option<Program>,
    shaders: Vec<(String, Program)>,
    init_function: Option<fn() -> T>,
    rendering_loop: Option<fn(&mut T, &mut Context)>,
    target_width: i32,
    target_height: i32,
    keep_aspect_ratio: bool,
}

impl<T> World<T> {
    pub fn init() -> World<T> {
        Self {
            glfw: Default::default(),
            window: Default::default(),
            events: Default::default(),
            default_shader: Default::default(),
            shaders: Default::default(),
            init_function: Default::default(),
            rendering_loop: Default::default(),
            target_width: Default::default(),
            target_height: Default::default(),
            keep_aspect_ratio: Default::default(),
        }
    }

    pub fn window(mut self, name: &str, width: u32, height: u32) -> Self {
        let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
        glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
        glfw.window_hint(glfw::WindowHint::OpenGlProfile(
            glfw::OpenGlProfileHint::Core,
        ));
        let (mut window, events) = glfw
            .create_window(width, height, name, glfw::WindowMode::Windowed)
            .expect("Failed to create the GLFW window.");
        glfw::Context::make_current(&mut window);
        window.set_key_polling(true);
        window.set_framebuffer_size_polling(true);
        gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);
        self.window = Some(RefCell::new(window));
        self.events = Some(RefCell::new(events));
        self.glfw = Some(RefCell::new(glfw));
        self.target_width = width as i32;
        self.target_height = height as i32;
        self
    }

    pub fn default_shader(mut self, vert_path: &str, frag_path: &str) -> Self {
        let program = Program::from_files(vert_path, frag_path).unwrap();
        self.default_shader = Some(program);
        self
    }

    pub fn add_shader(mut self, name: &str, vert_path: &str, frag_path: &str) -> Self {
        self.shaders.push((
            name.to_owned(),
            Program::from_files(vert_path, frag_path).unwrap(),
        ));
        self
    }

    pub fn init_func(mut self, f: fn() -> T) -> Self {
        self.init_function = Some(f);
        self
    }

    pub fn rendering_loop(mut self, f: fn(&mut T, &mut Context)) -> Self {
        self.rendering_loop = Some(f);
        self
    }

    pub fn keep_aspect_ratio(mut self) -> Self {
        self.keep_aspect_ratio = true;
        self
    }

    pub fn build(self) -> Self {
        let ortho = nalgebra_glm::ortho(
            0.,
            self.target_width as f32,
            0.,
            self.target_height as f32,
            -1.,
            1.,
        );
        for shader in self.shaders.iter() {
            shader.1.set_uniform_mat4("projectionMatrix", ortho);
        }
        self.default_shader.as_ref().unwrap().set_uniform_mat4("projectionMatrix", ortho);
        World { ..self }
    }

    pub fn start(self) -> Result<(), String> {
        let mut game_state = match self.init_function {
            Some(f) => f(),
            None => return Err("No init function provided.".to_string()),
        };
        self.default_shader.unwrap().set_used();
        let mut context = Context {
            current_shader: None,
            default_shader: self.default_shader.unwrap(),
            rendering_mode: Mode::Triangles,
            clear_color: [0.0, 0.0, 0.0, 0.0],
            shaders: self.shaders.to_owned(),
        };
        context.default_shader.set_used();
        self.main_loop(&mut game_state, &mut context)
    }

    fn main_loop(self, game_state: &mut T, context: &mut Context) -> Result<(), String> {
        let render_func = match self.rendering_loop {
            Some(f) => f,
            None => return Err("No rendering function provided.".to_string()),
        };
        let mut glfw = self.glfw.as_ref().unwrap().borrow_mut();
        let mut window = self.window.unwrap().into_inner();
        let events = self.events.as_ref().unwrap().borrow_mut();
        while !window.should_close() {
            let render_start = std::time::Instant::now();
            unsafe { gl::Clear(gl::COLOR_BUFFER_BIT) };
            render_func(game_state, context);
            glfw.poll_events();
            for (_, event) in glfw::flush_messages(&events) {
                match event {
                    glfw::WindowEvent::FramebufferSize(w, h) => {
                        if !self.keep_aspect_ratio {
                            break;
                        }
                        let (x, y, width, height) =
                            compute_viewport(w, h, self.target_width, self.target_height);
                        context.set_viewport(x, y, width, height);
                    }
                    _ => {}
                }
            }
            std::thread::sleep(
                std::time::Duration::from_millis(16 /* TODO default value */)
                    - render_start.elapsed(),
            );
            glfw::Context::swap_buffers(&mut window);
        }
        Ok(())
    }
}

fn compute_viewport(w: i32, h: i32, target_w: i32, target_h: i32) -> (i32, i32, i32, i32) {
    let ratio_x = w as f32 / target_w as f32;
    let ratio_y = h as f32 / target_h as f32;
    let x: i32;
    let y: i32;
    let width: i32;
    let height: i32;
    if ratio_x > ratio_y {
        width = (1920. as f32 * ratio_y) as i32;
        height = h;
        x = (w - width) / 2;
        y = 0;
    } else {
        width = w;
        height = (1080. as f32 * ratio_x) as i32;
        x = 0;
        y = (h - height) / 2;
    }
    (x, y, width, height)
}
