use crate::render::*;

pub struct Context {
    pub current_shader: Option<(String, Program)>,
    pub default_shader: Program,
    pub rendering_mode: Mode,
    // TODO Change this to a Vec4
    pub clear_color: [f32; 4],
    pub shaders: Vec<(String, Program)>
}

impl Context {
    pub fn set_shader(&mut self, prog: &str) {
        let p = self.shaders.iter().find(|x| x.0 == prog).expect(&format!("No registered shader named '{}'", prog));
        if self.current_shader.is_none() || p.1 != self.current_shader.as_ref().unwrap().1 {
            self.current_shader = Some(p.to_owned());
            p.1.set_used();
        }
    }

    pub fn set_default_shader(&mut self) {
        if self.current_shader.is_some() {
            self.current_shader = None;
            self.default_shader.set_used();
        }
    }

    pub fn set_rendering_mode(&mut self, mode: Mode) {
        self.rendering_mode = mode;
    }

    pub fn set_clear_color(&mut self, cc: [f32; 4]) {
        self.clear_color = cc;
        unsafe { gl::ClearColor(cc[0], cc[1], cc[2], cc[3]) };
    }

    pub fn set_viewport(&mut self, x: i32, y: i32, width: i32, height: i32) {
        unsafe {
            gl::Viewport(x, y, width, height);
        }
    }
}
