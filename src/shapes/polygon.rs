extern crate gl;

use crate::engine::Context;
use crate::render::*;

use gl::types::*;
use nalgebra_glm::*;

pub struct Polygon {
    points: Vec<(i32, i32)>,
    indices: Vec<GLuint>,
    vao: VAO,
    transform: Mat4,
    shader: Option<String>,
}

impl Polygon {
    pub fn from_points(points: Vec<(i32, i32)>) -> Polygon {
        let vbo = VBO::from_2d_points(&points);
        let indices = Self::triangulate(&points);
        let ebo = VBO::from_data::<GLuint>(&indices, gl::ELEMENT_ARRAY_BUFFER);
        let vao = VAO::new();
        vao.attach_vbo::<i32>(vbo, 0, gl::INT);
        vao.attach_ebo(ebo);
        Polygon {
            points,
            indices,
            vao,
            transform: identity(),
            shader: None,
        }
    }

    fn triangulate(points: &Vec<(i32, i32)>) -> Vec<GLuint> {
        /* TODO Triangulate the polygon to support concave polygons. */

        /* This is the "fan algorithm" that cna triangulate convex polygons. */
        let mut indices = Vec::new();
        let size = points.len() as GLuint;
        for i in 1..(size - 1) {
            indices.push(0);
            indices.push(i % size);
            indices.push((i + 1) % size);
        }
        indices
    }

    pub fn draw(&self, ctx: &mut Context) {
        match self.shader.as_ref() {
            Some(s) => ctx.set_shader(s),
            None => ctx.set_default_shader(),
        }
        let primitive = match ctx.rendering_mode {
            Mode::Triangles => gl::TRIANGLES,
            Mode::Wireframe => gl::LINE_LOOP,
        };
        unsafe {
            gl::BindVertexArray(self.vao.id);
            gl::DrawElements(
                primitive,
                self.indices.len() as i32,
                gl::UNSIGNED_INT,
                std::ptr::null(),
            )
        }
    }

    pub fn set_program(&mut self, prog: &str) {
        self.shader = Some(prog.to_owned());
    }
}

impl std::fmt::Debug for Polygon {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("Polygon")
            .field("points", &self.points)
            .field("indices", &self.indices)
            .field("transform", &self.transform)
            .finish()
    }
}
