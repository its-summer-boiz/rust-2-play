extern crate gl;

use gl::types::*;
use std::ops::Deref;

pub struct VBO {
    id: GLuint,
}

impl VBO {
    pub fn from_2d_points(points: &Vec<(i32, i32)>) -> VBO {
        let mut f_data: Vec<i32> = Vec::new();
        for point in points.iter() {
            f_data.push(point.0);
            f_data.push(point.1);
            f_data.push(0);
        }
        Self::from_data::<i32>(&f_data, gl::ARRAY_BUFFER)
    }

    pub fn from_data<T>(data: &Vec<T>, buffer_type: GLenum) -> VBO {
        let mut vbo = VBO { id: 0 };
        unsafe {
            gl::GenBuffers(1, &mut vbo.id);
            gl::BindBuffer(buffer_type, vbo.id);
            gl::BufferData(
                buffer_type,
                (data.len() * std::mem::size_of::<T>()) as GLsizeiptr,
                data.as_ptr() as *const GLvoid,
                gl::STATIC_DRAW,
            );
            gl::BindBuffer(buffer_type, 0);
        }
        vbo
    }
}

pub struct VAO {
    pub id: GLuint,
}

impl VAO {
    pub fn new() -> VAO {
        let mut vao = VAO { id: 0 };
        unsafe {
            gl::GenVertexArrays(1, &mut vao.id);
        }
        vao
    }

    pub fn attach_vbo<T>(&self, vbo: VBO, location: u32, data_type: GLenum) {
        unsafe {
            gl::BindVertexArray(self.id);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo.id);
            gl::VertexAttribPointer(
                location,
                3, //TODO Change later for multiple attributes
                data_type,
                0,
                (3 * std::mem::size_of::<T>()) as GLsizei, //TODO Change later as well
                std::ptr::null(),
            );
            gl::EnableVertexAttribArray(location);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }
    }

    pub fn attach_ebo(&self, ebo: VBO) {
        unsafe {
            gl::BindVertexArray(self.id);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo.id);
            gl::BindVertexArray(0);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
        }
    }
}

impl Deref for VAO {
    type Target = GLuint;

    fn deref(&self) -> &Self::Target {
        &self.id
    }
}
