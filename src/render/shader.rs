extern crate gl;

use gl::types::*;
use std::result::Result;

pub struct Shader {
    id: GLuint,
    gl_type: GLuint,
}

impl Shader {
    //TODO Change String to custom Error type
    pub fn from_file(path: &str, shader_type: GLuint) -> Result<Shader, String> {
        let mut s = Shader {
            id: 0,
            gl_type: shader_type,
        };
        let source = match std::fs::read_to_string(path) {
            Ok(string) => string,
            Err(error) => return Err(error.to_string()),
        };
        unsafe {
            s.id = gl::CreateShader(shader_type);
            let c_str_source = std::ffi::CString::new(source.as_bytes()).unwrap();
            gl::ShaderSource(s.id, 1, &c_str_source.as_ptr(), std::ptr::null());
            gl::CompileShader(s.id);
            let mut success = 0;
            gl::GetShaderiv(s.id, gl::COMPILE_STATUS, &mut success);
            if success != 1 {
                let mut length: i32 = 0;
                gl::GetShaderiv(s.id, gl::INFO_LOG_LENGTH, &mut length);
                let mut info_log = Vec::with_capacity(length as usize);
                gl::GetShaderInfoLog(
                    s.id,
                    length,
                    std::ptr::null_mut(),
                    info_log.as_mut_ptr() as *mut GLchar,
                );
                panic!("{}", std::str::from_utf8(&info_log).unwrap());
            }
        }
        Ok(s)
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct Program {
    id: GLuint,
}

impl Program {
    pub fn from_files(vs_source: &str, fs_source: &str) -> Result<Program, String> {
        let vs = Shader::from_file(vs_source, gl::VERTEX_SHADER).unwrap();
        let fs = Shader::from_file(fs_source, gl::FRAGMENT_SHADER).unwrap();
        Self::from_shaders(&vs, &fs)
    }

    pub fn from_shaders(vs: &Shader, fs: &Shader) -> Result<Program, String> {
        if vs.gl_type != gl::VERTEX_SHADER || fs.gl_type != gl::FRAGMENT_SHADER {
            return Err(
                "must provide a vertex shader and a fragment shader in the right order."
                    .to_string(),
            );
        }
        let mut p = Program { id: 0 };
        unsafe {
            p.id = gl::CreateProgram();
            gl::AttachShader(p.id, vs.id);
            gl::AttachShader(p.id, fs.id);
            gl::LinkProgram(p.id);
            let mut success = 0;
            gl::GetProgramiv(p.id, gl::LINK_STATUS, &mut success);
            if success != 1 {
                let mut length = 0;
                gl::GetProgramiv(p.id, gl::INFO_LOG_LENGTH, &mut length);
                let mut info_log = Vec::with_capacity(length as usize);
                gl::GetProgramInfoLog(
                    p.id,
                    length,
                    std::ptr::null_mut(),
                    info_log.as_mut_ptr() as *mut GLchar,
                );
                panic!("{}", std::str::from_utf8(&info_log).unwrap());
            }
            gl::DetachShader(p.id, vs.id);
            gl::DetachShader(p.id, fs.id);
        }
        Ok(p)
    }

    pub fn set_used(&self) {
        unsafe { gl::UseProgram(self.id) };
    }

    fn set_unused(&self) {
        unsafe { gl::UseProgram(0) };
    }

    pub fn set_uniform_mat4(&self, name: &str, val: nalgebra_glm::Mat4) {
        self.set_used();
        unsafe {
            let loc = gl::GetUniformLocation(
                self.id,
                std::ffi::CString::new(name).unwrap().as_ptr() as *const GLchar,
            );
            gl::UniformMatrix4fv(loc, 1, 0, val.as_ptr());
        }
        self.set_unused();
    }
}

impl std::fmt::Debug for Program {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("Program")
            .field("id", &self.id)
            .finish()
    }
}
