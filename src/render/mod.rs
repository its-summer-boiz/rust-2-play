mod shader;
mod vertex_objects;

pub use shader::*;
pub use vertex_objects::*;

pub enum Mode {
    Triangles,
    Wireframe,
}
